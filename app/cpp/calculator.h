#ifndef _CALCULATOR_H_
#define _CALCULATOR_H_

class Calculator {
public:
    int add(int a, int b);
    int multiply(int a, int b);
};

#endif
