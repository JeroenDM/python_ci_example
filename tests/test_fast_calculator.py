from app.cpp.calculator import Calculator

c = Calculator()

def test_add():
    assert c.add(3, 5) == 8

def test_multiply():
    assert c.multiply(3, 5) == 15
