[![pipeline status](https://gitlab.com/JeroenDM/python_ci_example/badges/master/pipeline.svg)](https://gitlab.com/JeroenDM/python_ci_example/commits/master)
[![coverage report](https://gitlab.com/JeroenDM/python_ci_example/badges/master/coverage.svg)](https://gitlab.com/JeroenDM/python_ci_example/commits/master)

# Pytest using gitlab ci

Experiment to run pytest automatically at each commit.